package com.Trello.Generic;

import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import com.Trello.pom.LoginPage;

public class BaseClass {
  public static WebDriver driver;

	@BeforeClass
	public void openBrowser() throws Throwable {
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://trello.com/home");
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
	}
	
	@BeforeMethod
	public void loginApp() throws Throwable {
		 
		LoginPage lp=new LoginPage(driver);
		lp.getLoginLink().click();
		 FileLib fl=new FileLib();
			String email=fl.getpropertydata("email");
			String password=fl.getpropertydata("password");
		lp.setLogin(email,password);
			
	}
	
	/* @AfterClass
	public void closeBrowser() {
		driver.manage().window().minimize();
		driver.close();
	} */
}
