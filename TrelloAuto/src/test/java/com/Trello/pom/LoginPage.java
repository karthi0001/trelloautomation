package com.Trello.pom;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

	@FindBy(linkText="Log in")
	private WebElement loginlink;
	
	@FindBy(id="username")
	private WebElement emailId;
	
	@FindBy(id="password")
	private WebElement pwdTextField;
	
	@FindBy(xpath = "//span[text()=\"Log in\"]")
	private WebElement loginBtn;
	
	public LoginPage(WebDriver driver) throws Throwable {
		PageFactory.initElements(driver, this);
		Thread.sleep(3000);
	
	}
	public WebElement getLoginLink() {
		return loginlink;
	}

	public void setLogin(String email, String password) {
		
		emailId.sendKeys(email);
		pwdTextField.sendKeys(password);
		
		
	}

	
}
